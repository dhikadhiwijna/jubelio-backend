
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS users;

-- public.product definition
CREATE TABLE public.product (
	id SERIAL PRIMARY KEY,
	description varchar(50) NULL,
	"name" varchar(128) NOT NULL,
	price int4 NOT NULL,
	sku varchar(50) NOT NULL,
	image varchar(255) NULL
);

-- public.user definition
CREATE TABLE public.users (
	id SERIAL PRIMARY KEY,
	user_id varchar(255) NOT NULL,
	"name" varchar(128) NOT NULL,
	email varchar(255) NOT NULL,
	password varchar(100) NOT NULL
);

INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Mobile Phone / Smartwatch','Baseus Cable 1 Meter Micro USB',5000,'BS010',NULL),
	 ('Men Clothing','222 - Black-1112',100000,'16LSOL-M0901001/M',NULL),
	 ('Moslem Fashion','Baju Magdalene',89000,'MAGDALENE_BLACK',NULL),
	 ('Mobile Phone / Smartwatch','TORU Motomo Aluminium Case for iPhone 6 Plus - Silver',200000,'TORU02',NULL),
	 ('Mobile Phone / Smartwatch','TORU Motomo Aluminium Case for iPhone 6 Plus - Dark Blue',200000,'TORU01',NULL),
	 ('Bags & Accessories','Tas selempang Biru Dongker',30000,'1203578196',NULL),
	 ('Mobile Phone / Smartwatch','SGP Neo Hybrid Ex Plastic Case for iPhone 6 Plus - Magenta',200000,'SPGN01',NULL),
	 ('Men Clothing','test el6',1000000,'tes-el6-Hit',NULL),
	 ('Men Clothing','test el6',1000000,'tes-el6-Hit',NULL),
	 ('Mobile Phone / Smartwatch','SGP Neo Hybrid Ex Plastic Case for iPhone 6 Plus - Gold',200000,'SPGN02',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Men Clothing','test el6',1000000,'tes-el6-Hit',NULL),
	 ('Mobile Phone / Smartwatch','SPG Full Armor Case for iphone 6 plus',200000,'SPG04',NULL),
	 ('Books & Magazines','test el5',1000000,'test el4',NULL),
	 ('Hobby','Pakaian Kucing Dan Anjing HOOPET  Warna GREY',150000,'HPET02',NULL),
	 ('Books & Magazines','test el5',1000000,'1609397612',NULL),
	 ('Babies & Kids Clothing','Pakaian Anjing Dan Kucing HOOPET warna SILVER',150000,'HPET03',NULL),
	 ('Home Decor, Bedding & Bath','Gembok anti maling warna silver',399000,'GBK2',NULL),
	 ('Hobby','Pakaian Anjing Dan Kucing HOOPET PERAK Atau SILVER',98000,'15Y0020GH0001',NULL),
	 ('Food & Beverages','kopi gayo',10000000,'kopigy1',NULL),
	 ('Mobile Phone / Smartwatch','NOOSY TPU Soft Case for iPhone 6 Plus - Transparant',100000,'NOOSY02',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Make Up & Skin Care','Baju Corak Carik Warna Warni',100000,'BLACK',NULL),
	 ('Men Clothing','Baju Kaos Polos Aneka Size',500000,'KAOS2',NULL),
	 ('Men Clothing','Celana Dalam Sepeda Cycling Pants Bantalan Gel Empuk',150000,'CD001',NULL),
	 ('Sports & Outdoor Activities','Celana Dalam Sepeda Cycling Pants Bantalan Gel Empuk',99000,'CD002',NULL),
	 ('Bags & Accessories','Coleman Packable Grass',230000,'COL-PAC-(GR-KUN',NULL),
	 ('Health, Body & Personal Care','Charm Body Fit Extra Maxi Wing 20pads  Test Only',79000,'U81818181818',NULL),
	 ('Mobile Phone / Smartwatch','Beli Casing HP iPhone 6 Plus 3 Gratis Casing 1',1000000,'SPGN02 - CSG001',NULL),
	 ('Mobile Phone / Smartwatch','Case Spigen Neo Hybrid Iphone 6 Plus - Red',200000,'SPGN01',NULL),
	 ('Make Up & Skin Care','Lipstick The One Lip Sensation Vynil Gell Glossy',10000,'1198654055',NULL),
	 ('Men Clothing','test el2',10000,'1609367863',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Mobile Phone / Smartwatch','SGP Slim Armor Plastic TPU Combination Case with Kickstand for iPhone 6 Plussss test',39000,'SPGK01',NULL),
	 ('Mobile Phone / Smartwatch','SGP Slim Armor Plastic TPU Combination Case with Kickstand for iPhone 6 Plussss test',39000,'SPGK01',NULL),
	 ('Mobile Phone / Smartwatch','TORU Motomo Aluminium Case for iPhone 6s Plusss test',149000,'MT01',NULL),
	 ('Mobile Phone / Smartwatch','Spigen Neo Full Armor Case Iphone 6s Plusssssssss test',150000,'1609475228',NULL),
	 ('Men Clothing','Stylebasic T Shirt Kaos  Polos Big Sizeeeeeee',19900,'BJ0019',NULL),
	 ('Men Clothing','Stylebasic T Shirt Kaos  Polos Big Sizeeeeeee',19900,'BJ0019',NULL),
	 ('Mobile Phone / Smartwatch','Ted Baker 19 Hard Case for iPhone 6s Plus',149000,'TD001',NULL),
	 ('Camera, Video Camera & Drone','Anker Karapax Casing Touch For IPhone X Clear',299500,'A9004H01',NULL),
	 ('Men Clothing','test el98 var',1000000,'tes-el9-var-Hit',NULL),
	 ('Books & Magazines','test el',3000000,'test el',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Men Clothing','Celana Cargo Panjang Sambung The North Face',320000,'JAK12353',NULL),
	 ('Men Clothing','Jaket Doopdry Navy',340000,'JAK12349',NULL),
	 ('Women Clothing','Sweater hoodie Hijacket wanita HJ Navy x turkish',370000,'JAK12359',NULL),
	 ('Babies & Kids Clothing','Baju Anak Perempuan Branded Setelan merk NEXT',100000,'JAK12355',NULL),
	 ('Women Clothing','Boston Terrier Blue Sweater',340000,'JAK12357',NULL),
	 ('Women Clothing','Jaket wanita pokemon terbaru',300000,'JAK12358',NULL),
	 ('Men Clothing','Jaket Hoodie Zipper EIGER',250000,'JAK32513',NULL),
	 ('Men Clothing','Jaket BOMBER INV VELIX',250000,'JAK45341',NULL),
	 ('Men Clothing','Jaket Hoodie',180000,'JAK43254',NULL),
	 ('Women Clothing','Jaket Cewe Hijaket Hijab Jaket Basic 18',270000,'JAK12360',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Moslem Fashion','Baju Muslim Anak Perempuan Jersey Peach Mint',370000,'JAK12344',NULL),
	 ('Women Clothing','Supreme polka Black',200000,'JASD3144',NULL),
	 ('Men Clothing','Jaket Bola Adidas Mayer Abu Terang',310000,'JAK45132',NULL),
	 ('Men Clothing','Jaket The North Face Venture Hybrid Men Ultralight Original Black',500000,'JAK12350',NULL),
	 ('Babies & Kids Clothing','Baju Anak Perempuan usia 4-5-6-7 tahun',10000,'JAK12354',NULL),
	 ('Men Clothing','Jaket Sweater Polos  Biru Dongker  Navy',150000,'KOPLSsss',NULL),
	 ('Men Clothing','Kaos Greenlight',80000,'Kao-Gre-Hit-L',NULL),
	 ('Shoes','Adidas Shoes',3500000,'Kao-Gre-Hit-M',NULL),
	 ('Men Clothing','Kaos Greenlight',80000,'Kao-Gre-Hit-N',NULL),
	 ('Men Clothing','Kaos Greenlight',80000,'Kao-Gre-Hit-O',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Men Clothing','Kaos 3 Seconds Size L',80000,'Kao-Gre-Hit-P',NULL),
	 ('Men Clothing','Kaos 3 Seconds Size L',80000,'Kao-Gre-Hit-Q',NULL),
	 ('Men Clothing','Update Product With Variants - Do Not Buy This Items',50000,'SKU-00001',NULL),
	 ('Men Clothing','Test Product With Variants - Do Not Buy This Items',50000,'SKU-00002',NULL),
	 ('Men Clothing','Test Product - Do Not Buy This Items',50000,'SKU-00003',NULL),
	 ('Men Clothing','Update Product With Variants - Do Not Buy This Items',50000,'SKU-00004',NULL),
	 ('Men Clothing','Test Product With Variants - Do Not Buy This Items',50000,'SKU-00005',NULL),
	 ('Men Clothing','Test Product With Variants - Do Not Buy This Items',50000,'SKU-00006',NULL),
	 ('Men Clothing','Test Product With Variants - Do Not Buy This Items',50000,'SKU-00007',NULL),
	 ('Men Clothing','Test Product - Do Not Buy This Items',50000,'SKU-00008',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Men Clothing','Test Product - Do Not Buy This Items',50000,'SKU-00009',NULL),
	 ('Men Clothing','TSHIRT XX764',10000,'SKU-00010',NULL),
	 ('Men Clothing','Test Product - Do Not Buy This Items',50000,'SKU-00011',NULL),
	 ('Mobile Phone / Smartwatch','Test jualabc',16000000,'1608101338',NULL),
	 ('Furniture','test abc2',10000000,'test abc1',NULL),
	 ('Furniture','test abc2',10000000,'tes abc2',NULL),
	 ('Books & Magazines','test abc4',10000000,'test abc3',NULL),
	 ('Books & Magazines','test abc4',10000000,'test abc4',NULL),
	 ('Books & Magazines','test abc4',10000000,'test abc5',NULL),
	 ('Books & Magazines','test abc4',10000000,'test abc6',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Books & Magazines','test abc4',10000000,'test abc7',NULL),
	 ('Books & Magazines','test abc4',10000000,'test abc8',NULL),
	 ('Books & Magazines','test abc4',10000000,'test abc9',NULL),
	 ('Men Clothing','testabc5',2000000,'use-XS',NULL),
	 ('Men Clothing','abc6',2000000,'abc-XZ',NULL),
	 ('Men Clothing','test baju 1',1000000,'tes-baj-1-XXX',NULL),
	 ('Men Clothing','143-3 a',100000,'tes abc15',NULL),
	 ('Men Clothing','test baju 1',1000000,'tes-baj-1-XXX',NULL),
	 ('Men Clothing','Kaos 3 Seconds Merah Size L 2nd',80000,'1604337175',NULL),
	 ('Bags & Accessories','TOPI SPORT',2000000,'1607311637',NULL);
INSERT INTO public.product (description,"name",price,sku,image) VALUES
	 ('Men Clothing','Kaos 3 Seconds Merah Size L',10000,'tes-lagi-2-xxx',NULL);

	