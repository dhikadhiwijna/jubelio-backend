# jubelio-backend

Server for e-commerce backend Application

## Getting started

1. Clone this repository
2. cd jubelio-backend
3. Create database connection in your PostgreSQL (You can use DBeaver)
4. Configure .env files in root to your database connection
5. Run SQL script "postgre_database_jubelio_test.sql" file in root folder to your database that you used.

## Run your Application

6. After table created in database and .env files configured. Run npm install to create modules
7. "npm start" to run your application

## Description

This Project is created for jubelio technical test.

## Problem related installation

1. Your connection to database is invalid.
2. Node modules error. Remove node_modules and run npm install
