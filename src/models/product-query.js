const pool = require("../config/database");

module.exports = {
  getProducts: (limit, offset) =>
    pool.query("SELECT * FROM product ORDER BY id DESC LIMIT $1 OFFSET $2", [
      limit,
      offset,
    ]),
  countProducts: () => pool.query("SELECT COUNT(*) as total FROM product"),
  getProductById: (id) =>
    pool.query("SELECT * FROM product WHERE id = $1", [id]),
  getProductByName: (name, limit) =>
    pool.query("SELECT * FROM product WHERE name ILIKE $1 LIMIT $2", [
      `%${name}%`,
      limit,
    ]),
  createProduct: (name, price, desc, sku, fileUpload) =>
    pool.query(
      "INSERT INTO product (name, price, description, sku, image) VALUES ($1, $2, $3, $4, $5)",
      [name, price, desc, sku, fileUpload]
    ),
  deleteProduct: (id) => pool.query("DELETE FROM product WHERE id = $1", [id]),
  updateProduct: (id, name, price, desc, sku, fileUpload) =>
    pool.query(
      "UPDATE product SET name = $1, price = $2, sku = $3, description = $4, image = $5 WHERE id = $6",
      [name, price, sku, desc, fileUpload, id]
    ),
  updateProductWithoutFile: (id, name, price, desc, sku) =>
    pool.query(
      "UPDATE product SET name = $1, price = $2, sku = $3, description = $4 WHERE id = $5",
      [name, price, sku, desc, id]
    ),
};
