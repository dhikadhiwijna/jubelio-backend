const pool = require("../config/database");

module.exports = {
  getUsers: () => pool.query("SELECT * FROM users ORDER BY id ASC"),
  getUserById: (id) =>
    pool.query("SELECT id, user_id, name, email FROM users WHERE id = $1", [
      id,
    ]),
  getUserByemail: (email) =>
    pool.query("SELECT * FROM users WHERE email = $1", [email]),
  createUser: (email, password, name, userId) =>
    pool.query(
      "INSERT INTO users (name, email, password, user_id) VALUES ($1, $2, $3, $4)",
      [name, email, password, userId]
    ),
  deleteUser: (id) => pool.query("DELETE FROM users WHERE id = $1", [id]),
  updateUser: (id, name, email, password) =>
    pool.query(
      "UPDATE users SET name = $1, password = $2, email=$3 WHERE id = $4",
      [name, password, email, id]
    ),
};
