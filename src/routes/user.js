const dbUser = require("../models/user-query");
const helper = require("../helper/emptyRow");
const Boom = require("@hapi/boom");

module.exports = [
  {
    method: "GET",
    path: "/users",
    handler: async (request, h) => {
      try {
        //QUERY GET ALL DATA
        const user = (await dbUser.getUsers()).rows;

        //HELPER CHECK EMPTY DATA
        const data = helper.emptyOrRows(user);

        if (!data.length) {
          return h.response("Data Not Found");
        }

        return h.response(data);
      } catch (error) {
        console.log(error);
        return h.response("/user error");
      }
    },
  },
  {
    method: "GET",
    path: "/users/{id}",
    handler: async (request, h) => {
      try {
        const { id } = request.params;

        //QUERY CHECK USER
        const userId = (await dbUser.getUserById(id)).rows;

        //HELPER CHECK EMPTY DATA
        const data = helper.emptyOrRows(userId);

        if (!data.length) {
          return Boom.badRequest("Data Not Found");
        }

        //RESPOND IF GET USER BY ID SUCCESS
        return h.response(data);
      } catch (error) {
        console.log(error);
        return h.response("/user/{id} error");
      }
    },
  },
];
