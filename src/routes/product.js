const helper = require("../helper/emptyRow");
const checkAuth = require("../helper/checkAuth");
const productQuery = require("../models/product-query");
const handleFileUpload = require("../helper/handleUpload");
const fs = require("fs");
const Boom = require("@hapi/boom");

const checkPersonPre = { method: checkAuth, assign: "auth" };

module.exports = [
  {
    method: "GET",
    path: "/products",
    config: {
      pre: [checkPersonPre],
    },
    handler: async (request, h) => {
      try {
        // const { auth } = request.pre;
        const limit = request.query.limit || 10;
        const offset = request.query.offset || 0;

        //PAGINATION
        const totalProduct = parseInt(
          (await productQuery.countProducts()).rows[0].total
        );
        const totalPage = Math.floor((await totalProduct) / limit);
        const currentPage = offset / limit + 1;

        //QUERY GET ALL DATA
        const product = (await productQuery.getProducts(limit, offset)).rows;

        //HELPER CHECK EMPTY DATA
        const data = helper.emptyOrRows(product);

        if (!product.length) {
          return h.response({ msg: "Data Not Found" });
        }

        //RESPOND IF GET ALL PRODUCT SUCCESS
        return h.response({
          per_page: limit,
          current_page: currentPage,
          total_page: totalPage,
          data: data,
        });
      } catch (error) {
        return h.response({ msg: `Server Error Product` });
      }
    },
  },
  {
    method: "GET",
    path: "/products/detail/{id}",
    config: {
      pre: [checkPersonPre],
    },
    handler: async (request, h) => {
      try {
        const { id } = request.params;

        //QUERY GET DATA BY ID
        const product = (await productQuery.getProductById(id)).rows;

        //HELPER CHECK EMPTY DATA
        const data = helper.emptyOrRows(product);

        if (!product.length) {
          return Boom.badRequest("Product not found");
        }

        //RESPOND IF GET ALL USER SUCCESS
        return h.response(data);
      } catch (error) {
        return Boom.badImplementation("DETAIL PRODUCT NOT FOUND");
      }
    },
  },
  {
    method: "GET",
    path: "/products/search/",
    config: {
      pre: [checkPersonPre],
    },
    handler: async (request, h) => {
      try {
        const { name } = request.query;
        const limit = request.query.limit || 10;

        //QUERY GET ALL DATA
        const product = (await productQuery.getProductByName(name, limit)).rows;

        //HELPER CHECK EMPTY DATA
        const data = helper.emptyOrRows(product);

        if (!product.length) {
          return Boom.badRequest("Product not found");
        }
        return h.response({
          data: data,
        });
      } catch (error) {
        return Boom.badImplementation("SERVER ERROR SEARCH NOT FOUND");
      }
    },
  },
  {
    method: "POST",
    path: "/products/create",
    config: {
      pre: [checkPersonPre],
      payload: {
        maxBytes: 209715200,
        output: "stream",
        parse: true,
        multipart: true,
      },
      //MIDDLEWARE TIDAK BISA DIGUNAKAN, TIDAK BISA DI CONFIG
      //   validate: {
      //     //VALIDATE INPUT DATA
      //     payload: Joi.object({
      //       name: Joi.string().min(5).max(100),
      //       sku: Joi.string().min(5).max(100),
      //       price: Joi.string().min(5).max(100),
      //       description: Joi.string().min(5).max(140),
      //     }),
      //   },
    },
    handler: async (request, h) => {
      try {
        const name = request.payload.name;
        const sku = request.payload.sku;
        const price = request.payload.price;
        const desc = request.payload?.description || null;
        const data = request.payload;
        let fileUpload = null;

        if (data.file.hapi.filename) {
          fileUpload = `http://localhost:4000/assets/${await handleFileUpload(
            data
          )}`;
        }

        // CREATE PRODUCT IN DATABASE
        const createProduct = await productQuery.createProduct(
          name,
          price,
          desc,
          sku,
          fileUpload
        );
        if (!createProduct) {
          return Boom.badImplementation("Failed to create product");
        }

        //RESPOND IF SIGN UP SUCCESS
        return h.response({
          message: `Product ${name} have been created`,
        });
      } catch (error) {
        return Boom.badImplementation("Create API Failed");
      }
    },
  },
  {
    method: "PUT",
    path: "/products/edit/{id}",
    config: {
      pre: [checkPersonPre],
      payload: {
        maxBytes: 209715200,
        output: "stream",
        parse: true,
        multipart: true,
      },
      //MIDDLEWARE TIDAK BISA DIGUNAKAN, TIDAK BISA DI CONFIG
      //   validate: {
      //     //VALIDATE INPUT DATA
      //     payload: Joi.object({
      //       name: Joi.string().min(5).max(100),
      //       sku: Joi.string().min(5).max(100),
      //       price: Joi.string().min(5).max(100),
      //       description: Joi.string().min(5).max(140),
      //     }),
      //   },
    },
    handler: async (request, h) => {
      try {
        const { id } = request.params;
        const name = request.payload.name;
        const sku = request.payload.sku;
        const price = request.payload.price;
        const desc = request.payload?.description || null;
        const data = request.payload || null;
        let fileUpload = null;

        fileUpload = data.file.hapi?.filename
          ? `http://localhost:4000/assets/${await handleFileUpload(data)}`
          : null;

        //QUERY GET DATA BY ID
        const product = (await productQuery.getProductById(id)).rows;

        //HELPER CHECK EMPTY DATA
        const isData = helper.emptyOrRows(product)[0];

        if (!product.length) {
          return Boom.badImplementation("Product not found");
        }

        // UPDATE PRODUCT IN DATABASE
        if (fileUpload !== null) {
          if (isData?.image) {
            let fileLength = isData.image.split("/").length;
            let filepath = isData.image
              .split("/")
              .slice(fileLength - 1)
              .join();
            // FILE WHICH WILL BE DELETED
            fs.unlinkSync(`./assets/${filepath}`);
          }

          const updateProduct = await productQuery.updateProduct(
            id,
            name,
            price,
            desc,
            sku,
            fileUpload
          );
          if (!updateProduct) {
            return Boom.badImplementation("Failed to create product");
          }
        } else {
          const updateProductWithoutFile =
            await productQuery.updateProductWithoutFile(
              id,
              name,
              price,
              desc,
              sku
            );
          if (!updateProductWithoutFile) {
            return Boom.badImplementation("Failed to create product");
          }
        }

        //RESPOND IF SIGN UP SUCCESS
        return h.response({
          message: `Product ${name} have been updated`,
        });
      } catch (error) {
        console.log(error);
        return Boom.badImplementation("Update API Failed");
      }
    },
  },
  {
    method: "DELETE",
    path: "/products/delete/{id}",
    config: {
      pre: [checkPersonPre],
    },
    handler: async (request, h) => {
      try {
        const { id } = request.params;

        //QUERY GET DATA BY ID
        const product = (await productQuery.getProductById(id)).rows;

        //HELPER CHECK EMPTY DATA
        if (!product.length) {
          return h.response("Product not found");
        }

        const isData = helper.emptyOrRows(product)[0];

        if (isData?.image) {
          let fileLength = isData.image.split("/").length;
          let filepath = isData.image
            .split("/")
            .slice(fileLength - 1)
            .join();
          // FILE WHICH WILL BE DELETED
          fs.unlinkSync(`./assets/${filepath}`);
        }

        //QUERY DELETE PRODUCT
        const deleteProduct = await productQuery.deleteProduct(id);
        if (!deleteProduct) {
          return Boom.badImplementation(`Failed to delete id ${id}`);
        }
        if (!deleteProduct) {
          return Boom.badImplementation("Failed to delete product");
        }

        //RESPOND IF UPDATE SUCCESS
        return h.response({
          message: `Product ${id} have been deleted`,
        });
      } catch (error) {
        console.log(error);
        return Boom.badImplementation("Delete API Error");
      }
    },
  },
];
