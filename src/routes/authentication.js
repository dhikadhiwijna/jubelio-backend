const Joi = require("joi");
const dbUser = require("../models/user-query");
const bcrypt = require("bcrypt");
const { randomUUID } = require("crypto");
// const helper = require("../helper/emptyRow");
const JWT = require("jsonwebtoken");
const Boom = require("@hapi/boom");

module.exports = [
  {
    method: "POST",
    path: "/auth/signup",
    options: {
      validate: {
        //VALIDATE INPUT DATA
        payload: Joi.object({
          name: Joi.string().min(5).max(80),
          email: Joi.string().min(5).max(100),
          password: Joi.string().min(5).max(140),
        }),
      },
    },
    handler: async (request, h) => {
      try {
        const { email, password, name } = request.payload;

        //CHECK USER IN DATABASE
        const checkUserExist = await dbUser.getUserByemail(email);

        if (checkUserExist.rows.length) {
          return Boom.badRequest(`${email} is already registered`);
        }

        //BCRYPT PASSWORD
        const saltRounds = 10;
        const hashedPassword = await bcrypt.hash(password, saltRounds);

        //CREATE USER IN DATABASE
        const userId = randomUUID();
        const userSignUp = await dbUser.createUser(
          email,
          hashedPassword,
          name,
          userId
        );

        if (!userSignUp) {
          return Boom.badImplementation(`Failed to create user for ${email}`);
        }

        //GET CREATED USER
        const getUser = await dbUser.getUserByemail(email);
        const dataUser = await getUser.rows[0];

        //CREATE TOKEN
        const publicKey = "jubelio";
        const token = JWT.sign(
          {
            user_id: dataUser.user_id,
            name: dataUser.name,
            email: dataUser.email,
          },
          publicKey,
          { expiresIn: 36000 }
        );
        h.state("token", token);

        //RESPOND IF SIGN UP SUCCESS
        return h.response({
          statusCode: 200,
          msg: "Account has been created",
          token: token,
        });
      } catch (error) {
        return h.response({ msg: `Server Error signup` });
      }
    },
  },
  {
    method: "POST",
    path: "/auth/signin",
    options: {
      validate: {
        //VALIDATE INPUT DATA
        payload: Joi.object({
          email: Joi.string().min(5).max(100),
          password: Joi.string().min(5).max(140),
        }),
      },
    },
    handler: async (request, h) => {
      try {
        const { email, password } = request.payload;

        //CHECK USER IN DATABASE
        const checkUserExist = await dbUser.getUserByemail(email);

        if (!checkUserExist.rows.length) {
          return Boom.badRequest(`${email} is not registered`);
        }

        //COMPARE BCRYPT: CHECK PASSWORD
        const userPassword = checkUserExist.rows.find(
          (user) => user.email === email
        ).password;

        const isMatch = await bcrypt.compare(password, userPassword);
        if (!isMatch) {
          return Boom.badRequest(`Wrong Password`);
        }

        //GET CREATED USER
        const dataUser = await checkUserExist.rows[0];

        //CREATE TOKEN
        const publicKey = "jubelio";
        const token = JWT.sign(
          {
            user_id: dataUser.user_id,
            name: dataUser.name,
            email: dataUser.email,
          },
          publicKey,
          { expiresIn: 36000 }
        );
        h.state("token", token);

        //RESPOND IF SIGN UP SUCCESS
        return h.response({
          statusCode: 200,
          msg: "Account is ready to use",
          token: token,
        });
      } catch (error) {
        return h.response({ msg: `Server Error signin` });
      }
    },
  },
  {
    method: "GET",
    path: "/signout",
    handler: async (request, h) => {
      const tokenCookies = request.state.token;

      if (tokenCookies) {
        h.unstate("token");
      }
      return h.response({ cookies: request.state });
    },
  },
];
