const user = require("./user");
const product = require("./product");
const auth = require("./authentication");

module.exports = [].concat(user, auth, product);
