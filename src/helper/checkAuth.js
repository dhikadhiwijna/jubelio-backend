const Boom = require("@hapi/boom");
const JWT = require("jsonwebtoken");
module.exports = async (request) => {
  //GET TOKEN
  const token = await request.headers?.token;

  if (!token) {
    return Boom.badRequest("No Token Found");
  }

  //VERIFY TOKEN
  try {
    const publicKey = "jubelio";
    const user = await JWT.verify(token, publicKey);

    if (!user) {
      Boom.badGateway("Token Expired");
    }

    return { user_id: user.user_id, name: user.name, email: user.email };
  } catch (error) {
    Boom.badImplementation("Token Invalid");
  }
};
