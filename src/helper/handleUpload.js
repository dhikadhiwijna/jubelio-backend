const fs = require("fs");
const Boom = require("@hapi/boom");
module.exports = (data) => {
  try {
    const name = `${Date.now()}_${data.file.hapi.filename}`;
    const dir = __dirname.split("/").length;
    const dirn = __dirname
      .split("/")
      .slice(0, dir - 2)
      .join("/");

    const path = dirn + "/assets/" + name;
    const file = fs.createWriteStream(path);

    file.on("error", (err) => console.error(err));

    data.file.pipe(file);

    return name;
  } catch (error) {
    Boom.badImplementation("File Upload Error");
  }
};
