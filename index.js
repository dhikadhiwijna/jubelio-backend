"use strict";

const Hapi = require("@hapi/hapi");
const routes = require("./src/routes");

const server = Hapi.server({
  port: 4000,
  host: "localhost",
  routes: {
    cors: {
      origin: ["*"],
      headers: [
        "Accept",
        "Authorization",
        "Content-Type",
        "If-None-Match",
        "Accept-language",
        "token",
      ],
    },
  },
});

server.route(routes);
server.state("token", {
  ttl: null,
  isSecure: true,
  isHttpOnly: true,
});

server.route({
  method: "GET",
  path: "/",
  handler: (request, h) => {
    return h.response("Welcome to Elevania API 🤙🤙🤙🤙🤙🤙");
  },
});

server.route({
  method: "GET",
  path: "/{file*}",
  handler: (request, h) => {
    const { file } = request.params;

    return h.file(file);
  },
});

const init = async () => {
  await server.register([
    {
      plugin: require("hapi-geo-locate"),
      options: {
        enabledByDefault: true,
      },
    },
    { plugin: require("@hapi/inert") },
  ]);
  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
